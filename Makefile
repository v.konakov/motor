CXX=g++
CF=-Wwrite-strings -fprofile-arcs -ftest-coverage
LSET=-lSDL2main -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lpthread -lyaml -lphysfs
LDIR=-L/usr/local/lib
INCSDL=-I/usr/local/include/SDL2
INCYML=-I/usr/local/include/
INCMTR=-Isrc/
BIN=MotorTest
TEST=TimedMotorTest
LOG=log.txt

MotorTest:
	$(CXX) $(CF) $(INCSDL) $(INCYML) $(INCMTR) -o $(BIN) src/testbed.cpp $(LDIR) $(LSET)
	@echo Build done

clean:
	rm -f $(BIN)
	rm $(LOG)
	./bin/clean
	@echo Clean done

test:
	$(CXX) $(CF) $(INCSDL) $(INCYML) $(INCMTR) -o $(TEST) src/timedtestbed.cpp $(LDIR) $(LSET)
	@echo Timed test build done

cleantest:
	rm -f $(TEST)
	rm $(LOG)
	./bin/clean
	@echo Test clean done
