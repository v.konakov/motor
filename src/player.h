#ifndef PLAYER
#define PLAYER

#include "mains.h"
#include "motor.h"

class Player : public Asset
{
    public:
        int Status;
    public:
        Player();
        ~Player();
        void HandleInput();
        void ShowMove();
};

Player::Player()
{
    HandleInput();
}

Player::~Player()
{

}

void Player::HandleInput()
{
    PollEvent();

    if (MainEvent.type == SDL_KEYDOWN) {
        switch (MainEvent.key.keysym.sym) {
            case SDLK_RIGHT:
                Velocity += DestR.w;
                std::cout << "Right key pressed\n";
                break;
            case SDLK_LEFT:
                Velocity -= DestR.w;
                std::cout << "Left key pressed\n";
                break;
        }
    }
    else if (MainEvent.type == SDL_KEYUP) {
        switch (MainEvent.key.keysym.sym) {
            case SDLK_RIGHT: Velocity -= DestR.w; break;
            case SDLK_LEFT: Velocity += DestR.w; break;
        }
    }

}

void Player::ShowMove()
{
    DestR.w += Velocity;
    SDL_RenderCopy(GetRenderer(), TextureOpt, &SrcR, &DestR);
    SDL_RenderPresent(GetRenderer());
}

#endif
