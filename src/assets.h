#ifndef ASSETS
#define ASSETS

#include "mains.h"
#include "motor.h"
#include "window.h"
#include "filesystem.h"

class Asset : public Filesystem
{
    public:
        SDL_Texture *TextureOpt;
        SDL_Surface *TempSurface;
        SDL_Rect SrcR;
        SDL_Rect DestR;
        const std::string File;
        int SpriteID;
        int Velocity;
        int SpriteHeight;
        int SpriteWidth;
        int SpriteXOfst;
        int SpriteYOfst;
    public:
        Asset();
        ~Asset();
        void DestTexture();
        void DrawSprite();
        int LoadSprite(int SpriteID, const std::string File);
        int SetRects(int SpriteHeight, int SpriteWidth, int SpriteXOfst, int SpriteYOfst);
};

Asset::Asset()
{
    Velocity = 0;
}

Asset::~Asset()
{
    DestTexture();
}

int Asset::LoadSprite(int SpriteID, const std::string File)
{
    SetBaseDir("testsprites.zip");
    FileChck(File.c_str());
    TempSurface = IMG_Load(File.c_str());
    IMGErrorPrint();
    TextureOpt = SDL_CreateTextureFromSurface(GetRenderer(), TempSurface);

    if (&TempSurface != NULL) {
        SDL_FreeSurface(TempSurface);
        std::cout << "SpriteID: " << SpriteID << "\tFreed surface\n";
    }
    else if (&TempSurface == NULL) {
        std::cout << "SpriteID: " << SpriteID << "\tNo surface to free\n";
        SDLErrorPrint();
        return (-1);
    }

    if (&TextureOpt == NULL) {
        std::cout << "SpriteID: " << SpriteID << "\tCould not load sprite\n";
        SDLErrorPrint();
        return (-2);
    }
    else if (&TextureOpt != NULL) {
        std::cout << "SpriteID: " << SpriteID
            << "\tSprite " << File << " loaded.\n";
        return (0);
    }
}

int Asset::SetRects(int SpriteHeight, int SpriteWidth, int SpriteXOfst, int SpriteYOfst)
{
    SrcR.x = 0;
    SrcR.y = 0;
    SrcR.h = SpriteHeight;
    SrcR.w = SpriteWidth;
    DestR.x = SpriteXOfst;
    DestR.y = SpriteYOfst;
    DestR.h = SpriteHeight;
    DestR.w = SpriteWidth;
    return 0;
}

void Asset::DestTexture()
{
    if (TextureOpt != NULL) {
        SDL_DestroyTexture(TextureOpt);
        std::cout << "Texture " << File << " destroyed\n";
    }
    else if (TextureOpt == NULL) {
        std::cout << "No texture to destroy\n";
        SDLErrorPrint();
    }
}

void Asset::DrawSprite()
{
    SDL_RenderCopy(GetRenderer(), TextureOpt, &SrcR, &DestR);
    SDL_RenderPresent(GetRenderer());
}

#endif
